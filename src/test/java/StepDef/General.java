package StepDef;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.simple.JSONObject;
import org.junit.BeforeClass;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import util.driver_utility;
import util.excel_utility;
import util.static_data;

public class General 
{
	
/*	@BeforeTestRun
	public static void beforeallscenario() throws Exception
	{
		//Pre_Run.test();
		System.out.println("**************HIIIIIIIIIIIIIIIIIIIIIIIIIIIII****************************");
	}*/
	
	//static String[] pre_config_data = new String[100];	
	
	@Before
	public void Retrieving_Feature_Scenario_Name(Scenario scenario)
	{		
		static_data.currentRunningScenarioName = scenario.getName();		
		static_data.currentRunningScenario = scenario;
		
		static_data.currentRunningFeatureName = scenario.getId().split(";")[0].replace("-"," ");
		
		System.out.println("*************Current Running Feature is : "+static_data.currentRunningFeatureName+"*******************");
		System.out.println("*************Current Running Scenario is : "+static_data.currentRunningScenarioName+"*******************");		
		
	}
	
	@Before
	public void DataSetup()
	{
		if(!static_data.execution_start_counter)
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
			Date date = new Date();
			static_data.today_date = dateFormat.format(date);
			
			static_data.environment = excel_utility.getcellvaluebyName("Environment Under Test",static_data.pre_config_sheet);
			static_data.url = excel_utility.getcellvaluebyName(static_data.environment,static_data.url_sheet);
			static_data.capturescreenshot = excel_utility.getcellvaluebyName("Capture Screenshot",static_data.pre_config_sheet);
			static_data.browser = excel_utility.getcellvaluebyName("Browser Under Test",static_data.pre_config_sheet);
			static_data.BaseReportingDirectory = excel_utility.getcellvaluebyName("Log Path",static_data.pre_config_sheet);
			static_data.short_sleep_time = Integer.parseInt(excel_utility.getcellvaluebyName("Short Page Load Time",static_data.pre_config_sheet));
			static_data.long_sleep_time = Integer.parseInt(excel_utility.getcellvaluebyName("Long Page Load Time",static_data.pre_config_sheet));
			static_data.TestDataFilePath = static_data.TestDataFilePath + static_data.environment + ".xlsx";	
								
			static_data.execution_start_counter=true;
		}
	}
	
	@After
	public void Closing_Driver_Instance() throws UnknownHostException, IOException
	{		
		if(static_data.driver!=null)
			driver_utility.close_sessionInstance();	
		
		//report();
					
	}
	
	
	public void report() throws UnknownHostException, IOException 
	{
		
		JSONObject logMap = new JSONObject();
		
	    Date endDate = Calendar.getInstance().getTime();
        DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
	     
	    logMap.put("TC Name", static_data.currentRunningScenarioName);
	    logMap.put("result", "Pass");
	    logMap.put("timestamp", df.format(endDate));
	    logMap.put("os", "Windows");
	    logMap.put("build_number", "1234");
	    
	    String a = logMap.toJSONString();
	    
	    System.out.println("a is : "+a);
		
		
		
		
		Socket socket = new Socket("127.0.0.1", 5401);
		DataOutputStream os = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
		//String a = "{\"result\":\"passed\",\"TC Name\":\"TC06\",\"os\":\"Windows\",\"build_number\":\"4567\",\"timestamp\":\"02-02-2019 22:04:44\"}\n";
		os.writeBytes(a);
		//os.writeBytes("{\"result\":\"passed\",\"TC Name\":\"TC05\",\"os\":\"Windows\",\"build_number\":\"4567\",\"timestamp\":\"02-02-2019 22:04:44\"}\n");
		os.flush();
		socket.close();
	}
	
	
/*	public void createReportingDirectory()
	{
		System.out.println(static_data.FinalReportingDirectory);
		
		File file = new File(static_data.FinalReportingDirectory);
        if (!file.exists()) 
        {
            if (file.mkdirs()) 
            {
                System.out.println("Reporting Directory is created!");
            } 
            else 
            {
                System.out.println("Failed to create Reporting Directory!");
            }
        }
	}*/
	
}
