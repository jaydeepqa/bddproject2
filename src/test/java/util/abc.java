package util;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.simple.JSONObject;

public class abc {

	public static void main(String[] args) throws UnknownHostException, IOException 
	{
		
		JSONObject logMap = new JSONObject();
		
	    Date endDate = Calendar.getInstance().getTime();
        DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
	     
	    logMap.put("TC Name", "TC03");
	    logMap.put("result", "Fail");
	    logMap.put("timestamp", df.format(endDate));
	    logMap.put("os", "Windows");
	    logMap.put("build_number", "1234");
	    
	    String a = logMap.toJSONString();
	    
	    System.out.println("a is : "+a);
		
		
		
		
		Socket socket = new Socket("127.0.0.1", 5400);
		DataOutputStream os = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
		//String a = "{\"result\":\"passed\",\"TC Name\":\"TC06\",\"os\":\"Windows\",\"build_number\":\"4567\",\"timestamp\":\"02-02-2019 22:04:44\"}\n";
		os.writeBytes(a);
		//os.writeBytes("{\"result\":\"passed\",\"TC Name\":\"TC05\",\"os\":\"Windows\",\"build_number\":\"4567\",\"timestamp\":\"02-02-2019 22:04:44\"}\n");
		os.flush();
		socket.close();
	}

}
