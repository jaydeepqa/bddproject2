package util;

import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class driver_utility 
{
	//static String user_dir = System.getProperty("user.dir");
	
	public static void start_sessionInstance()
	{
		try
		{
			
			String USERNAME = "jay1027";
			String AUTOMATE_KEY = "W5rDbThkyiqJQ2BEzGzD";
			String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

		    DesiredCapabilities caps = new DesiredCapabilities();
		    caps.setCapability("browser", "Chrome");
		    caps.setCapability("browser_version", "64.0");
		    caps.setCapability("os", "Windows");
		    caps.setCapability("os_version", "10");
		    caps.setCapability("resolution", "1024x768");
		    
		    static_data.driver = new RemoteWebDriver(new URL(URL), caps);
		    static_data.driver.manage().window().maximize();
			
			static_data.wait = new WebDriverWait(static_data.driver,static_data.long_sleep_time);
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
				
	}

	
	public static void close_sessionInstance()
	{
		System.out.println("*******************Closing the Browser*******************");
		
		static_data.driver.close();
		static_data.driver.quit();
		static_data.driver=null;
		static_data.wait=null;
	}

}

